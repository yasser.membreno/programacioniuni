public class MarcadorDemo{
   public static void main(String[] args) {
       Marcador bic = null;//objeto declarado

       bic = new Marcador("Rojo",2,"Permanente");
       bic.setColor("Azul");
       bic.setTamano(1);
       bic.setTipo("Acrilico");
       System.out.println("Color: " + bic.getColor());
       System.out.println("Tamano: " + bic.getTamano());
       System.out.println("Tipo: " + bic.getTipo());

   }
}