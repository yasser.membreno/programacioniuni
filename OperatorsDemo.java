public class OperatorsDemo{
    public static void main(String[] args) {
        arithmeticOPerations();

        int i = -1;

        int j = i++; // j = 0; i = 1  for(int i=0;i<10;++i){}
        int k = ++i; // k = 2; i = 2 

        System.out.println("j: " + j);
        System.out.println("k: " + k);
        System.out.println("i: " + i);
    }

    public static void arithmeticOPerations(){
        int x = 9, y = 34;

        int suma = x + y;        
        int resta = x - y;
        float division = (float) y / x;
        int multiplicacion = x * y;
        int modulo = y % x;

        suma = suma + x;// 43 + 9
        suma += x; // 43 + 9        
        boolean flag = !(suma > 100) && (suma < 50);

        suma = flag ? 200 : suma;
        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("division: " + division);
        System.out.println("Multiplicacion: " + multiplicacion);
        System.out.println("Modulo: " + modulo);
        System.out.println("flag: " + flag);
    }
}

/*
Aritmeticos:
    +,-,*,/,%
Asignaciones:
    =, +=, -=, *=, /=, %= 
Relacionales:
    <,>,<=,>=, ==, !=, instanceof  
Logicos:
    &&, ||, ^
Unarios:
    ++,--,!,+,-,~
Ternario:
  variable  = (condicion) ? valor1 : valor2
Desplazamientos de bits:
    >>, << , >>>
*/