public class Calculadora{

    public static void main(String[] args) {
        int x = 0, y = 0;
        if(args.length != 2){
            System.err.println("Error, digite exactamente dos parametros!");
        }else{
            x = Integer.parseInt(args[0]);
            y = Integer.parseInt(args[1]);
            
            //System.out.println("x: " + x + " y: " + y); 
            System.out.format("Suma: %d, x:%d, y:%d \n", suma(x,y), x, y);
        }
    }

    public static int suma(int a, int b){
        return (a+b);
    }
}